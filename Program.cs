﻿using System;

namespace exercise22
{
    class Program
    {
        static void Main(string[] args)
        {
            var movies = new string[5] {"red", "warcraft", "starwars","batman", "lord of the rings,"};
            Array.Sort(movies);
            movies[0] = "beetlejuice";
            movies[2] = "rockinroller";
            Console.WriteLine(movies[0]);
            Console.WriteLine(movies[1]);
            Console.WriteLine(movies[2]);
            Console.WriteLine(movies[3]);
            Console.WriteLine(movies[4]);
           var moviesL = movies.Length;
           Console.WriteLine(moviesL);
        }
    }
}
